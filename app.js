const express = require("express");
const hoganExpress = require("hogan-express");
const app = express();

app.use(express.static("frontend"))
app.engine('html', hoganExpress);
app.set('view options', { layout: true });
app.set('layout', 'container');
app.set('views', __dirname + "/frontend");
app.set('view engin', 'html');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

app.get("/", (req, res) => {
    res.render("index");
})

app.listen(3000, () => {
    console.log("app listen on 3000");
})


// 18605005004
// 44066666